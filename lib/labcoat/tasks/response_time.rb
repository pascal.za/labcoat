require 'faraday'
require 'benchmark'

module Labcoat
  module Tasks
    class ResponseTime
      TIMEOUT_IN_SECONDS = 10
      DEFAULT_NUMBER_OF_POLLS = 6
      DEFAULT_POLL_INTERVAL_IN_SECONDS = 10

      def initialize(number_of_polls: DEFAULT_NUMBER_OF_POLLS, poll_interval: DEFAULT_POLL_INTERVAL_IN_SECONDS)
        @number_of_polls = [number_of_polls, 1].max
        @poll_interval = poll_interval
      end

      def measure
        Benchmark.realtime do
          response = connection.head('/')

          raise Labcoat::GitLabUnreachable, "The response code (#{response.status}) was not successful" unless response.success?
        end
      rescue Faraday::ConnectionFailed => e
        raise Labcoat::GitLabUnreachable, e
      end

      def measure_average
        # Don't count first response as initial TCP+TLS negotiation is slow
        measure

        response_times = Array.new(@number_of_polls) do
          measure.tap do
            sleep(@poll_interval)
          end
        end

        response_times.inject(&:+) / @number_of_polls
      end

      private
      def connection
        @connection ||= Faraday.new(url: Labcoat::GITLAB_URL,
                                    request: {
                                      open_timeout: TIMEOUT_IN_SECONDS,
                                      timeout: TIMEOUT_IN_SECONDS
                                    }) do |faraday|
          faraday.adapter :net_http_persistent
        end
      end
    end
  end
end

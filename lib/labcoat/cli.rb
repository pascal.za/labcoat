require 'thor'
require 'labcoat/tasks/response_time'

module Labcoat
  class CLI < Thor
    desc "response_time", "Checks the average response time of gitlab.com (in milliseconds)"
    def response_time
      average_in_ms = (Labcoat::Tasks::ResponseTime.new.measure_average * 1000).round

      print average_in_ms.to_s
    rescue Labcoat::GitLabUnreachable => e
      warn "Unable to reach #{GITLAB_URL}: #{e}"
    end
  end
end

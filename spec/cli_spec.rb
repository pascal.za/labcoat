RSpec.describe Labcoat::CLI do
  context 'response time' do
    it 'reports on successful measurement' do
      allow_any_instance_of(Labcoat::Tasks::ResponseTime).to receive(:measure_average).and_return(1.5)

      expect {
        described_class.new.invoke(:response_time)
      }.to output("1500").to_stdout
    end

    it 'reports an error on reachability problems' do
      allow_any_instance_of(Labcoat::Tasks::ResponseTime).to receive(:measure_average).
                            and_raise(Labcoat::GitLabUnreachable.new('Zombies were unleashed'))

      expect {
        described_class.new.invoke(:response_time)
      }.to output("Unable to reach #{Labcoat::GITLAB_URL}: Zombies were unleashed\n").to_stderr
    end
  end
end

RSpec.describe Labcoat::Tasks::ResponseTime do
  context "pulling gitlab.com" do
    it "returns a time on a successful request" do
      allow_any_instance_of(Faraday::Connection).to receive(:head).and_return(
        double(success?: true, body: '')
      )

      expect(described_class.new.measure).to be > 0.0
    end

    it 'raises an error when the site is unreachable' do
      allow_any_instance_of(Faraday::Connection).to receive(:head).and_raise(
        Faraday::ConnectionFailed.new("Timeout")
      )

      expect {
        described_class.new.measure
      }.to raise_error(Labcoat::GitLabUnreachable)
    end
  end


  it "returns the average response time over a minute" do
    subject = described_class.new(number_of_polls: 2, poll_interval: 0)
    allow(subject).to receive(:measure).and_return(0.0, 0.1, 0.5)

    expect(subject.measure_average).to eq(0.3)
  end
end

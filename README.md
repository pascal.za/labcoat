![Labcoat Logo](doc/labcoat.png)

# Labcoat

[![pipeline status](https://gitlab.com/pascal.za/labcoat/badges/master/pipeline.svg)](https://gitlab.com/pascal.za/labcoat/commits/master) [![coverage report](https://gitlab.com/pascal.za/labcoat/badges/master/coverage.svg)](https://gitlab.com/pascal.za/labcoat/commits/master)

Labcoat is a command-line interface for performing various checks on GitLab's website.

## Installation

Install it from RubyGems as:

    $ gem install labcoat

## Usage

For information on the available LabCoat commands, run it with no arguments:

    $ labcoat

```
Commands:
  labcoat help [COMMAND]  # Describe available commands or one specific command
  labcoat response_time   # Checks the average response time of gitlab.com (in milliseconds)
```

### Website response time

To measure the response time from the machine running Labcoat, use the following command:

    $ labcoat response_time

This will output the average response time (by default tested every 10 seconds for a minute) in milliseconds, which can be piped to another command as desired.


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

### Command locations

To view or edit the entry point for a CLI command, look at [lib/labcoat/cli.rb](lib/labcoat/cli.rb) which implements commands using the Thor gem.

Individual tasks are added to the `lib/tasks` directory. Have a look at [lib/labcoat/tasks/response_time.rb](lib/labcoat/tasks/response_time.rb) for an example.

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/pascal.za/labcoat.

To submit fixes:

1. Fork this repository
1. Implement the desired feature with tests (and documentation if necessary)
1. Double check that the CI pipeline succeeds with a build
1. Send me a merge request

For new features, please start merge requests as an issue before beginning any implementation, so the maintainers can give early feedback and give some guidelines on how to best complete your contribution.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

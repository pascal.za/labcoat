
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "labcoat/version"

Gem::Specification.new do |spec|
  spec.name          = "labcoat"
  spec.version       = Labcoat::VERSION
  spec.authors       = ["Pascal Houliston"]
  spec.email         = ["101pascal@gmail.com"]

  spec.summary       = %q{CLI that performs various checks on gitlab.com}
  spec.description   = %q{Command line interface to GitLab used to perform various checks on their website}
  spec.homepage      = "https://gitlab.com/pascal.za/labcoat"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "bin"
  spec.executables   = %w{labcoat}
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "thor"
  spec.add_runtime_dependency "net-http-persistent"
  spec.add_runtime_dependency "faraday"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "simplecov", "~> 0.15"
end
